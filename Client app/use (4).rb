Shoes.setup do
  gem 'savon'
end

require 'savon'
client = Savon::Client.new(wsdl: "https://test-st-myapp.herokuapp.com/rumbas/wsdl")
ALL_LISTS = ["num", "name", "author", "type", "publisher", "isbn", "price", "page","thick","thprice","remove"]
LISTS_SIZE = [50, 200, 100, 200, 120, 150, 80, 50, 70, 100, 100, 40] # last for margin
ATTRIBUTES = ["Name", "Author", "Type", "Publisher", "ISBN" , "Thick" , "Thprice"]

class Hash
  def to_xml(prefix)
    res = self[(prefix+"_response").to_sym][:value]
    res = Nokogiri::XML res
    return res
  end
end

Shoes.app :width=>790, :height=>550, title:"~Pomegranate's Book Store~" do

  def ValueInput(name)
    result = {}
    flow do
      result[:choose] = "Between"
      result[:name] = name
      result[:search_lbl] = para name+" : \t\t\t"
      result[:search_list] = list_box items: ["At", "Greater than", "Less than", "Between"], choose: "Between", width: 100
      para '  '
      result[:search_edit1] = edit_line width: 100
      result[:and_text] = para '  Between  '
      result[:search_edit2] = edit_line width: 100

      result[:search_list].change {
        |i|
        choose = i.text
        result[:choose] = choose
        if choose == "Between"
          result[:search_edit2].show
          result[:and_text].show
        else
          result[:search_edit2].hide
          result[:and_text].hide
        end
      }
    end
    return result
  end

  def TextInput(name)
    result = {}
    hide = false
    flow do
      result[:search_lbl] = para name+" : #{name == 'Publisher' ? "\t":"\t\t"}", hidden: hide
      result[:search_edit] = edit_line :width => 400, hidden: hide
    end
    return result
  end

  # def Table(list_data, color=black, width=50, stroke=1)
  #   @Table = []
  #   list_data.each {
  #       |i|
  #     @Table.push []
  #     flow do
  #       @Rows = []
  #       i.each { |j|
  #         flow width:width do
  #           @Rows.push({})
  #           @Rows[@Rows.length-1][:text] = para j
  #           @Rows[@Rows.length-1][:border] = border color, strokewidth:stroke
  #         end
  #       }
  #       @Table.push @Rows
  #     end
  #   }
  #   return @Table
  # end

  stack do
    # Set Object
    @bg = background rgb(0xE0, 0xC6, 0x85)
    @pic=image "http://lifeleaders.us/resources/banner.jpg"
    @title = title "Pomegranate's Bookstore" , align: 'center'
    @option = list_box items: ["Search", "Add", "Remove"], choose: "Search"
    inscription ' '
    @text = {}
    for i in ATTRIBUTES
      @text[i.to_sym] = TextInput(i)
    end
    @price = ValueInput("Price")
    @page = ValueInput("Page")
    @button = button("Submit")

    # Set Feature

    @option.change {
      |i| choose = i.text
      if choose == 'Add'
        @price[:search_list].items = ["As"]
        @price[:search_list].choose item: "As"
        @page[:search_list].items = ["As"]
        @page[:search_list].choose item: "As"
      else
        @price[:search_list].items = ["At", "Greater than", "Less than", "Between"]
        @price[:search_list].choose item: "Between"
        @page[:search_list].items = ["At", "Greater than", "Less than", "Between"]
        @page[:search_list].choose item: "Between"
      end
    }
    # @t = Table([['akamsdklmaslkd', 'b'], ['c', 'd']])

    @button.click {
      choose = @option.text.downcase
      data = { :by => 0b00000000, :name => nil, :author => nil, :type => nil,
               :publisher => nil, :isbn => nil, :price => nil, :page => nil, :thick => nil , :thprice => nil}
      add = 1
      @text.each { |key, value|
        v = value[:search_edit].text
        k = key.downcase
        if v != ''
          if choose == 'add'
            data[k] = v
          else
            data[:by] |= add
            if k == :ISBN
              data[k] = k.to_s + "=" + v
            else
              data[k] = k.to_s + "='" + v + "'"
            end
          end
        end
        add *= 2
      }
      list = [ @price, @page]
      key = [ :price, :page]
      for i in (0..1)
        text1 = list[i][:search_edit1].text
        text2 = list[i][:search_edit2].text
        opt = list[i][:search_list].text
        if text1 == '' or (opt == 'Between' and text2 == '')
          add *= 2
          next
        end
        if opt == 'At'
          data[:by] |= add
          data[key[i]] = key[i].to_s + "=" + text1
        elsif opt == 'As'
          data[key[i]] = text1
        elsif opt == 'Greater than'
          data[:by] |= add
          data[key[i]] = key[i].to_s + ">" + text1
        elsif opt == 'Less than'
          data[:by] |= add
          data[key[i]] = key[i].to_s + "<" + text1
        elsif opt == 'Between'
          data[:by] |= add
          data[key[i]] = key[i].to_s + ">=" + text1 + " and "\
                         + key[i].to_s + "<=" + text2
        end
        add *= 2
      end
      print data
      if choose == 'search'
        result = client.call(choose.to_sym, :message => data)
        result = result.to_hash.to_xml(choose)
        lists = result.xpath("//result").children.first
        window width:LISTS_SIZE.inject(0, :+), height:480 do
          background rgb(0xE0, 0xC6, 0x85)
          n = 0
          stack do
            title "~Search result~", align: 'center'
            flow do
              para '    '
              ALL_LISTS.each {
                |i|
                flow width:LISTS_SIZE[n] do
                  para i.capitalize
                  border black
                end
                n += 1
              }
            end
            num = 1
            while lists != nil
              n = 1
              flow do
                margin = para '    '
                element = {}
                border = {}
                flow width:LISTS_SIZE[0], fill:white do
                  element[:num] = para num.to_s
                  border[:num] = border black
                end
                for attr in ALL_LISTS[1..ALL_LISTS.length-1]
                  flow width:LISTS_SIZE[n], fill:white do
                    if attr == "remove"
                      s_button = button width:LISTS_SIZE[n]-5, margin:5 do
                        if confirm ("Want to remove?")
                          add = 1
                          by = 0
                          ALL_LISTS[1..ALL_LISTS.length-2].each { |value|
                            if element[value.to_sym].text != ''
                              by |= add
                            end
                            add *= 2
                          }
                          @s_data = {:by => by,
                                     :name => "name='"+element[:name].text+"'",
                                     :author => "author='"+element[:author].text+"'",
                                     :type => "type='"+element[:type].text+"'",
                                     :publisher => "publisher='"+element[:publisher].text+"'",
                                     :isbn => "isbn="+element[:isbn].text,
                                     :price => "price="+element[:price].text,
                                     :page => "page="+element[:page].text,
				     :thick => "thick="+element[:thick].text,
				     :price => "price="+element[:price].text}
                          print @s_data
                          result = client.call(:remove, :message => @s_data)
                          result = result.to_hash.to_xml("remove")
                          puts result.xpath("//result").children.to_s.to_i
                          if result.xpath("//result").children.to_s.to_i > 0
                            margin.hide
                            s_button.hide
                            element.each do
                            |i, j| j.hide
                            end
                            border.each do
                            |i, j| j.hide
                            end
                            alert("Remove Complete", :title => "Complete")
                          end
                        end
                      end
                    else
                      element[attr.to_sym] = para lists.css(attr).text
                    end
                    border[attr.to_sym] = border black
                  end
                  n += 1
                end
              end
              num += 1
              lists = lists.next_element
            end
          end
        end
      elsif choose == 'add'
        if confirm ("Want to add this?\n" +
            "Name : " + data[:name] + "\n" +
            "Author : " + data[:author] + "\n" +
            "Type : " + data[:type] + "\n" +
            "Publisher : " + data[:publisher] + "\n" +
            "ISBN : " + data[:isbn] + "\n" +
            "Price : " + data[:price] + " $" + "\n" +
            "Page : " + data[:page] + "\n"+
	    "Thick : " + data[:thick] + "\n"+
	    "Thprice : "+data[:thprice] + "\n")
          result = client.call(choose.to_sym, :message => data)
          result = result.to_hash.to_xml(choose)
          alert("Add Complete" + "\n" +
                    "Name : " + data[:name] + "\n" +
                    "Author : " + data[:author] + "\n" +
                    "Type : " + data[:type] + "\n" +
                    "Publisher : " + data[:publisher] + "\n" +
                    "ISBN : " + data[:isbn] + "\n" +
                    "Price : " + data[:price] + " $" + "\n" +
                    "Page : " + data[:page] + "\n" +
		    "Thick : " + data[:thick] + "\n" +
		    "Thprice : " +data[:thprice] , :title => "Complete")
        end

      elsif choose == 'remove'
        if confirm ("Want to remove?")
          client.call(choose.to_sym, :message => data)
          alert("Remove Complete", :title => "Complete")
        end
      end
    }
  end
end
